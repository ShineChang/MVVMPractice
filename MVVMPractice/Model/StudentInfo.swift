//
//  StudentInfo.swift
//  MVVMPractice
//
//  Created by i9400503 on 2019/6/20.
//  Copyright © 2019 Brille. All rights reserved.
//

import Foundation

protocol StudentInfo {
    var ID : String? { get set }
    var Name : String? {get set}
}
