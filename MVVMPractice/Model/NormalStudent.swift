//
//  NormalStudent.swift
//  MVVMPractice
//
//  Created by i9400503 on 2019/6/20.
//  Copyright © 2019 Brille. All rights reserved.
//

import Foundation

struct NormalStudent : StudentInfo{
    
    var ID : String?
    var Name : String?
    var UniteExamGrade : String?
    var HighSchoolName : String?
}
